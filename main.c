#include "LPC11XX.h"
#include <stdio.h>
#include "uart.h"
#include "led.h"
#include "i2c.h"
#include "i2c_eeprom.h"
#include "interrupt_handler_utility.h"
#include "i2c_oled.h"

#define BUFFER_SIZE 64

// Global variable


int main(void)
{
	//int i,j;
	//uint8_t data;
	uint8_t read_buffer[BUFFER_SIZE];
	//uint8_t write_buffer[BUFFER_SIZE];
	
	LedOutputCfg();
	UartConfig();
	i2cConfig();
	// setting SysTick frequency 1KHz
	SysTick_Config(48000);
	
	LedOn();
 	printf("\n*************\n");
	printf("*  I2C Test *\n");
	printf("*************\n");
	
	ADDRESS_SIZE = ONE_BYTE_ADDRESS;
	init_oled();
	oled_test(0);
	
	delay_ms(1000);
	oled_test(1);
	
	delay_ms(1000);
	oled_test(2);
	
	delay_ms(1000);
	oled_test(3);
	
	delay_ms(1000);
	oled_test(4);
	
//	LedOff();
//	while (1);
	
	ADDRESS_SIZE = TWO_BYTES_ADDRESS;
	
//	// ---------- byte write ------------------
//	printf("write to address: %d\n", 12288);
//	for (int i=0; i<64; i++) {
//		i2c_eeprom_byte_write(SLAVE_ADDRESS, 12288+i, i);
//		//delay_ms(5);
//		i2c_eeprom_ack_polling(SLAVE_ADDRESS);
//	}
	
	// ----------- page write -----------------
	for (int i=0; i<64; i++)
		read_buffer[i] = 64 - i;
	i2c_eeprom_page_write(SLAVE_ADDRESS, 13312, read_buffer, PAGE_SIZES);
	
	delay_ms(5);
	i2c_eeprom_ack_polling(SLAVE_ADDRESS);
	
	// ---------- SEQUENTIAL READ -------------
	i2c_eeprom_write_address(SLAVE_ADDRESS, 13312);
	i2c_eeprom_sequential_read(SLAVE_ADDRESS, read_buffer, BUFFER_SIZE);
	
	for (int i=0; i<BUFFER_SIZE; i++) {
		printf("%02X ", (uint8_t)read_buffer[i]);
		//read_buffer[i] = 0;  // clear buffer
	}
	printf("\n");
	
	// ------------ CURRENT ADDRESS READ -------------
	
//	printf("\n Current Address Read \n");
//	i2c_eeprom_write_address(SLAVE_ADDRESS, 12288);
//	i2c_eeprom_current_read(SLAVE_ADDRESS, &data);
//	printf("%02X \n", data);
//	i2c_eeprom_current_read(SLAVE_ADDRESS, &data);
//	printf("%02X \n", data);
//	i2c_eeprom_current_read(SLAVE_ADDRESS, &data);
//	printf("%02X \n", data);
//	i2c_eeprom_current_read(SLAVE_ADDRESS, &data);
//	printf("%02X \n", data);
//	i2c_eeprom_current_read(SLAVE_ADDRESS, &data);
//	printf("%02X \n", data);
//	
//	// ------------ RANDOM ADDRESS READ -------------
//	printf("\n Random Address Read \n");
//	i2c_eeprom_random_read(SLAVE_ADDRESS, 0, read_buffer, 1);
//	printf("%02X \n", read_buffer[0]);
//	i2c_eeprom_random_read(SLAVE_ADDRESS, 3, read_buffer, 1);
//	printf("%02X \n", read_buffer[0]);
//	
//	i2c_eeprom_random_read(SLAVE_ADDRESS, 0, read_buffer, BUFFER_SIZE - 1);
//	for (i=0; i<BUFFER_SIZE - 1; i++) {
//		printf("%02X ", (uint8_t)read_buffer[i]);
//		//read_buffer[i] = 0;  // clear buffer
//	}
	
	
	
	LedOff();
	while (1);
}



