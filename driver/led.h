#ifndef _LED_H
#define _LED_H

// Prototype declaration
void LedOutputCfg(void);
void LedOn(void);
void LedOff(void);
void LedToggle(void);

#endif
